FROM node:14.0 as build
WORKDIR /build
COPY source/package*.json ./
RUN npm install
COPY source/. ./
RUN npm run build

FROM nginx:1.18-alpine
COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build /build/dist /usr/share/nginx/html
