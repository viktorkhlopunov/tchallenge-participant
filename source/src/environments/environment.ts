export const environment = {
    apiBaseUrl: 'http://' + window.location.host,
    clientBaseUrl: 'http://localhost:4200',
    production: true
};